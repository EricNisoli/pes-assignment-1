'use strict';

const assert = require('assert');
const worker = require('../worker');
const downloadFile = require('download-file');
const url = 'http://hgdownload.cse.ucsc.edu/goldenPath/hg38/chromosomes/chr1_KI270707v1_random.fa.gz';
const options = {
    directory: `${process.env.FILES_DIR}/`,
    filename: "genome.fa.gz"
};

const download = () => {

    return new Promise((resolve, reject) => {

        downloadFile(url, options, (err) => {
            if (err)
            {
                reject(err);
            }
            else
            {
                resolve();
            }
        });
    });
};

describe('Worker', () => {

    it('should compute the request',  async () => {
        
        const dl = await download();
        const res = await worker.exec(options.filename, 3);
        assert(res !== undefined);
    });
});
