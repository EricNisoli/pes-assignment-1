'use strict';

const fs = require('fs');
const checksum = require('checksum');

exports.saveFile = (data, path) => {

    const file = fs.createWriteStream(path);

    return new Promise((resolve, reject) => {
        
        data.pipe(file);
        data.on('end', (err) => {
            if (err)
            {
                reject();
            }
            else
            {
                resolve();
            }
        });
    });
};

exports.hash = (path) => {

    return new Promise((resolve, reject) => {

        checksum.file(path, async (err, sum) => {
            
            if (err)
            {
                reject();
            }
            else
            {
                resolve(sum);
            }
        });
    });
};
