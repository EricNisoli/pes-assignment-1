'use strict';

const axios = require('axios');
const WORKER_HOST = process.env.WORKER_HOST || 'localhost';
const WORKER_PORT = process.env.WORKER_PORT || '3000';

exports.exec = async (filename, kmer) => {

    const url = 
        `http://${WORKER_HOST}:${WORKER_PORT}/compute/${filename}/${kmer}`;
    const response = await axios.get(url);

    return response.data;
};
