CREATE TABLE IF NOT EXISTS hashes (
    md5 TEXT,
    kmer INTEGER,
    graph TEXT,

    PRIMARY KEY (md5, kmer)
);
