'use strict';

const Hapi = require('hapi');

const server = Hapi.server({
    port: process.env.HTTP_PORT || 3000
});

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

const init = async () => {

    await server.register(require('inert'));

    const STATIC_FOLDER = './public';
    const CSS_FOLDER = `${STATIC_FOLDER}/css`;
    const JS_FOLDER = `${STATIC_FOLDER}/js`;
    const JQUERY_FOLDER = './node_modules/jquery/dist';
    const BOOTSTRAP_FOLDER = './node_modules/bootstrap/dist';

    server.route({
        method: 'GET',
        path: '/',
        handler: async (request, h) => {
    
            return h.file(`${STATIC_FOLDER}/index.html`);
        }
    });

    server.route({
        method: 'GET',
        path: '/css/bootstrap/{file}',
        handler: async (request, h) => {
    
            return h.file(`${BOOTSTRAP_FOLDER}/css/${request.params.file}`);
        }
    });

    server.route({
        method: 'GET',
        path: '/js/jquery/{file}',
        handler: async (request, h) => {
    
            return h.file(`${JQUERY_FOLDER}/${request.params.file}`);
        }
    });

    server.route({
        method: 'GET',
        path: '/js/bootstrap/{file}',
        handler: async (request, h) => {
    
            return h.file(`${BOOTSTRAP_FOLDER}/js/${request.params.file}`);
        }
    });

    server.route({
        method: 'GET',
        path: '/css/{file}',
        handler: async (request, h) => {
    
            return h.file(`${CSS_FOLDER}/${request.params.file}`);
        }
    });

    server.route({
        method: 'GET',
        path: '/js/{file}',
        handler: async (request, h) => {
    
            return h.file(`${JS_FOLDER}/${request.params.file}`);
        }
    });

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

init();
