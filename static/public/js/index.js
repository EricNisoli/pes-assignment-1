jQuery(($) => {

    $('form#upload').submit(() => {

        const kmer = $('#kmer').val();
        $('form#upload').attr('action', `/api/compute/${kmer}`);
    });
});
